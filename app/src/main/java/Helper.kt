
import android.text.TextUtils
import android.util.Patterns


class Helper {
    companion object {

        fun isValidEmail(target: CharSequence?): Boolean {

            return if (TextUtils.isEmpty(target)) {
                false
            } else {
                Patterns.EMAIL_ADDRESS.matcher(target).matches()
            }
        }
    }
}