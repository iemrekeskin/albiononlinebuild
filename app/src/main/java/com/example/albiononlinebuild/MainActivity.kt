package com.example.albiononlinebuild

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.google.firebase.auth.FirebaseAuth

private lateinit var firebaseAuth: FirebaseAuth
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        firebaseAuth = FirebaseAuth.getInstance()
        val logoutbutton = findViewById<Button>(R.id.logoutbutton)

        logoutbutton.setOnClickListener {
            firebaseAuth.signOut()
            checkUser()
        }

    }

    private fun checkUser() {
        val firebaseUser = firebaseAuth.currentUser
        if ( firebaseUser != null ){

        }else{
            //user is null , user is not logged in , intent log activity
            startActivity(Intent(this,LoginActivity::class.java))
            finish()
        }
    }
}