package com.example.albiononlinebuild

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.view.View
import android.widget.Button
import android.widget.TextClock
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseUser


class LoginActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private var email = ""
    private var password = ""

    private lateinit var progressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        progressDialog = ProgressDialog(this)
        progressDialog.setTitle ("Please wait")
        progressDialog.setCanceledOnTouchOutside(false)

        auth = Firebase.auth

        val loginbutton = findViewById<Button>(R.id.loginbutton)
        loginbutton.setOnClickListener {
            validateData()
             }
        
        val registertext =findViewById<TextView>(R.id.registertext)
        registertext.setOnClickListener {
            val intent = Intent (this , RegisterActivity::class.java)
            startActivity(intent) }

            auth = FirebaseAuth.getInstance()
            checkUser()
            }

    private fun validateData() {

        email = findViewById<TextView>(R.id.loginemail).text.toString().trim()
        password = findViewById<TextView>(R.id.loginpassword).text.toString().trim()
        //validate
        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            findViewById<TextView>(R.id.loginemail).error = "Lütfen geçerli bir e-posta giriniz"
        }
        else if (TextUtils.isEmpty(password)){
            findViewById<TextView>(R.id.loginpassword).error = "Lütfen şifrenizi giriniz"
        }
        else {
            firebaseLogin()
        }


    }

    private fun firebaseLogin() {
        auth.signInWithEmailAndPassword(email,password)

            .addOnSuccessListener {
               //login success
                val firebaseUser = auth.currentUser
                val email = firebaseUser!!.email
                Toast.makeText(this,"Logged as $email",Toast.LENGTH_SHORT).show()
                startActivity(Intent(this,MainActivity::class.java))
                finish()
            }
            .addOnFailureListener { e->

                progressDialog.dismiss()
                Toast.makeText(this,"Login failed due to ${e.message}",Toast.LENGTH_SHORT).show()
            }
    }

    private fun checkUser() {
        val firebaseUser = auth.currentUser
        if ( firebaseUser != null ){
            startActivity(Intent(this,MainActivity::class.java))
            finish()
        }
    }
}






