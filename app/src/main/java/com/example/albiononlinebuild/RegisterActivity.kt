package com.example.albiononlinebuild

import Helper
import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.example.albiononlinebuild.databinding.ActivityMainBinding
import com.example.albiononlinebuild.databinding.ActivityRegisterBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase


class RegisterActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth;
    lateinit var binding: ActivityMainBinding
    @SuppressLint("WrongViewCast")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
// Initialize Firebase Auth
        auth = Firebase.auth
        val registerButton = findViewById<Button>(R.id.registerbutton)
        val registerEmail = findViewById<TextView>(R.id.registeremail)
        val registerPassword = findViewById<TextView>(R.id.registerpassword)
        val registerPassword2 = findViewById<TextView>(R.id.registerpassword2)
        val registerusername = findViewById<TextView>(R.id.registerusername)
        val ref = FirebaseAuth.getInstance()




        var database =FirebaseDatabase.getInstance().reference

        registerPassword.text = "123456789"
        registerPassword2.text = "123456789"
        registerEmail.text = "asdasd@gmail.com"
        registerButton.setOnClickListener {


            when {
                TextUtils.isEmpty(registerEmail.text.toString().trim(' ')) -> {
                    Toast.makeText(
                        this@RegisterActivity,
                        "Please Enter an Email",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                TextUtils.isEmpty(registerPassword.text.toString().trim(' ')) -> {
                    Toast.makeText(
                        this@RegisterActivity,
                        "Please enter a password",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                else -> {
                    if (registerPassword.text.length < 8) {
                        Toast.makeText(
                            this,
                            " Şifre için en az 8 karakter giriniz.",Toast.LENGTH_SHORT).show()
                    } else if (registerPassword.text.toString() != registerPassword2.text.toString()) {
                        Toast.makeText(this, " Girilen sifreler birbirleriyle eslesmiyor.", Toast.LENGTH_SHORT).show()
                    } else {

                        if (Helper.isValidEmail(registerEmail.text)){

                         FirebaseAuth.getInstance().createUserWithEmailAndPassword(registerEmail.text.toString(),registerPassword.text.toString())

                               .addOnCompleteListener { task ->

                                   if (task.isSuccessful) {

                                       val firebaseUser: FirebaseUser = task.result!!.user!!

                                       var username = findViewById<TextView>(R.id.registerusername)
                                       var usermail = findViewById<TextView>(R.id.registeremail)
                                       var userpassword = findViewById<TextView>(R.id.registerpassword)

                                       var usersTable = database.child( "Users").child(firebaseUser.uid.toString())
                                           usersTable.child("username").setValue(username.text.toString())
                                           usersTable.child("email").setValue(usermail.text.toString())


                                       Toast.makeText(
                                           this,
                                           "KAYIT OLMA İŞLEMİ BAŞARILI",
                                           Toast.LENGTH_SHORT
                                       ).show()

                                       val intent = Intent(this, LoginActivity::class.java)
                                       startActivity(intent)


                                   } else {

                                       Toast.makeText(
                                           this,
                                           task.exception!!.message.toString(),
                                           Toast.LENGTH_SHORT
                                       ).show()
                                       Log.d("REGISTER",task.exception!!.message.toString())
                                   }
                               }


                        }
                            else {
                                Toast.makeText(this, "Lutfen gecerli bir eposta giriniz.",Toast.LENGTH_SHORT).show()
                        }
                    }



                }
            }


            //setValue(users(usermail.toString(),username.toString(),userpassword.toString()))

        }

    }
}
